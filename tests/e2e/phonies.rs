use crate::e2e::*;

// Repro for issue #84: phony depending on phony.
#[test]
fn phony_depends() -> anyhow::Result<()> {
    let space = TestSpace::new()?;
    space.write(
        "build.ninja",
        &[
            TOUCH_RULE,
            "
build out1: touch
build out2: phony out1
build out3: phony out2
",
        ]
        .join("\n"),
    )?;
    space.run_expect(&mut n2_command(vec!["out3"]))?;
    space.read("out1")?;
    Ok(())
}

#[test]
fn phony_with_missing_input_fails_build() -> anyhow::Result<()> {
    let space = TestSpace::new()?;
    space.write(
        "build.ninja",
        "
build out1: phony out2
",
    )?;
    let out = space.run(&mut n2_command(vec!["out1"]))?;
    assert_output_contains(&out, "input out2 missing\n");
    assert!(!out.status.success());
    Ok(())
}


#[cfg(unix)]
#[test]
fn real_rule_can_depend_on_phony() -> anyhow::Result<()> {
    let space = TestSpace::new()?;
    space.write(
        "build.ninja",
        "
rule touch
  command = touch $out
build out1: touch
build out2: phony out1
build out3: touch out2
",
    )?;
    space.run_expect(&mut n2_command(vec!["out3"]))?;
    space.read("out1")?;
    assert!(space.read("out2").is_err());
    space.read("out3")?;

    // rerunning shouldn't do anything. This makes sure that out2 doesn't
    // get a constantly-changing timestamp.
    let out = space.run_expect(&mut n2_command(vec!["out3"]))?;
    assert_output_contains(&out, "no work to do");
    Ok(())
}

#[cfg(unix)]
#[test]
fn phony_as_alias_for_input_files() -> anyhow::Result<()> {
    let space = TestSpace::new()?;
    space.write(
        "build.ninja",
        "
rule touch
  command = touch $out
build out1: phony in1 in2
build out2: touch out1
",
    )?;
    space.write("in1", "foo")?;
    space.write("in2", "foo")?;
    space.run_expect(&mut n2_command(vec!["out2"]))?;
    assert!(space.read("out1").is_err());
    space.read("out2")?;

    // rerunning shouldn't do anything.
    let out = space.run_expect(&mut n2_command(vec!["out2"]))?;
    assert_output_contains(&out, "no work to do");

    // Updating an input file and rerunning should cause a rebuild
    space.write("in1", "bar")?;
    let out = space.run_expect(&mut n2_command(vec!["out2"]))?;
    assert_output_not_contains(&out, "no work to do");

    Ok(())
}

#[cfg(unix)]
#[test]
fn phony_as_alias_for_generated_files() -> anyhow::Result<()> {
    let space = TestSpace::new()?;
    space.write(
        "build.ninja",
        "
rule touch
  command = touch $out
build out1: touch
build out2: phony out1
build out3: touch out2
",
    )?;
    let out = space.run_expect(&mut n2_command(vec!["out3"]))?;
    space.read("out1")?;
    assert!(space.read("out2").is_err());
    space.read("out3")?;
    assert_output_contains(&out, "ran 2 tasks");

    // rerunning shouldn't do anything.
    let out = space.run_expect(&mut n2_command(vec!["out3"]))?;
    assert_output_contains(&out, "no work to do");

    // Deleting the generated file and rebuilding should rebuild both files
    space.delete("out1")?;
    let out = space.run_expect(&mut n2_command(vec!["out3"]))?;
    assert_output_contains(&out, "ran 2 tasks");

    Ok(())
}

#[cfg(unix)]
#[test]
fn simple_phony_output() -> anyhow::Result<()> {
    let space = TestSpace::new()?;
    space.write(
        "build.ninja",
        "
rule echo
  command = echo Hello && touch side_effect.txt

build out: echo
  phony_output = true
",
    )?;
    let out = space.run_expect(&mut n2_command(vec!["out"]))?;
    assert_output_contains(&out, "ran 1 task");
    space.read("side_effect.txt")?;

    // Rerunning should still run the command
    let out = space.run_expect(&mut n2_command(vec!["out"]))?;
    assert_output_contains(&out, "ran 1 task");
    space.read("side_effect.txt")?;

    // Also demonstrate this via deleting the side_effect file and seeing it
    // get recreated.
    space.delete("side_effect.txt")?;
    let out = space.run_expect(&mut n2_command(vec!["out"]))?;
    assert_output_contains(&out, "ran 1 task");
    space.read("side_effect.txt")?;

    Ok(())
}

#[cfg(unix)]
#[test]
fn real_build_cannot_depend_on_phony_output() -> anyhow::Result<()> {
    use anyhow::bail;

    let space = TestSpace::new()?;
    space.write(
        "build.ninja",
        "
rule echo
  command = echo Hello

build out: echo
  phony_output = true

build out2: echo out
",
    )?;
    let out = space.run(&mut n2_command(vec!["out2"]))?;
    if out.status.success() {
        bail!("expected error, got success");
    }
    assert_output_contains(&out, "real file 'out2' depends on phony output 'out'");

    Ok(())
}
