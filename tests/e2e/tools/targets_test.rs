use crate::e2e::{n2_command, TestSpace};

#[test]
fn depth() -> anyhow::Result<()> {
    let space = TestSpace::new()?;
    space.write(
        "build.ninja",
        "
rule myrule

build a: myrule b c d
build b: myrule
build c: myrule
build d: myrule e
build e: myrule

build other_top_level: myrule
",
    )?;
    let output = space.run_expect(&mut n2_command(vec!["-t", "targets"]))?;
    assert_eq!(String::from_utf8_lossy(&output.stdout), "a: myrule
other_top_level: myrule
");

    let output = space.run_expect(&mut n2_command(vec!["-t", "targets", "depth"]))?;
    assert_eq!(String::from_utf8_lossy(&output.stdout), "a: myrule
other_top_level: myrule
");

    let output = space.run_expect(&mut n2_command(vec!["-t", "targets", "depth", "2"]))?;
    assert_eq!(String::from_utf8_lossy(&output.stdout), "a: myrule
  b: myrule
  c: myrule
  d: myrule
other_top_level: myrule
");

    let output = space.run_expect(&mut n2_command(vec!["-t", "targets", "depth", "0"]))?;
    assert_eq!(String::from_utf8_lossy(&output.stdout), "a: myrule
  b: myrule
  c: myrule
  d: myrule
    e: myrule
other_top_level: myrule
");

    Ok(())
}

#[test]
fn all() -> anyhow::Result<()> {
    let space = TestSpace::new()?;
    space.write(
        "build.ninja",
        "
rule myrule
rule myrule2

build a: myrule b c d
build b: myrule
build c: myrule2
build d: myrule2 e
build e: myrule

build other_top_level: myrule
",
    )?;
    let output = space.run_expect(&mut n2_command(vec!["-t", "targets", "all"]))?;
    assert_eq!(String::from_utf8_lossy(&output.stdout), "a: myrule
b: myrule
c: myrule2
d: myrule2
e: myrule
other_top_level: myrule
");

    Ok(())
}


#[test]
fn rule() -> anyhow::Result<()> {
    let space = TestSpace::new()?;
    space.write(
        "build.ninja",
        "
rule myrule
rule myrule2

build a: myrule b c d in1
build b: myrule in2 in3
build c: myrule2
build d: myrule2 e
build e: myrule in3

build other_top_level: myrule
",
    )?;
    let output = space.run_expect(&mut n2_command(vec!["-t", "targets", "rule", "myrule2"]))?;
    assert_eq!(String::from_utf8_lossy(&output.stdout), "c
d
");

    let output = space.run_expect(&mut n2_command(vec!["-t", "targets", "rule"]))?;
    assert_eq!(String::from_utf8_lossy(&output.stdout), "in1
in2
in3
");
    Ok(())
}
