//! Parsing of Makefile syntax as found in `.d` files emitted by C compilers.

use crate::{
    scanner::{ParseResult, Scanner},
    smallmap::SmallMap,
};

/// Skip spaces and backslashed newlines.
fn skip_spaces(scanner: &mut Scanner) -> ParseResult<()> {
    loop {
        match scanner.read() {
            ' ' => {}
            '\\' => match scanner.read() {
                '\r' => scanner.expect('\n')?,
                '\n' => {}
                _ => return scanner.parse_error("invalid backslash escape"),
            },
            _ => {
                scanner.back();
                break;
            }
        }
    }
    Ok(())
}

/// Read one path from the input scanner.
/// Note: treats colon as a valid character in a path because of Windows-style
/// paths, but this means that the inital `output: ...` path will include the
/// trailing colon.
fn read_path<'a>(scanner: &mut Scanner<'a>) -> ParseResult<Option<String>> {
    skip_spaces(scanner)?;
    let start = scanner.ofs;
    let mut result = String::new();
    loop {
        match scanner.read() {
            '\0' | ' ' | '\r' | '\n' => {
                scanner.back();
                break;
            }
            '\\' => {
                if scanner.peek_newline() {
                    scanner.back();
                    break;
                }
                // eat one more character.
                match scanner.read() {
                    // Escaped characters.
                    ' ' => result.push(' '),
                    '\\' => result.push('\\'),
                    '#' => result.push('#'),
                    '*' => result.push('*'),
                    '[' => result.push('['),
                    '|' => result.push('|'),
                    ']' => result.push(']'),
                    // End-of-file
                    '\0' => {
                        result.push('\\');
                        scanner.back();
                    }
                    // Otherwise, keep the "\".
                    c2 => {
                        result.push('\\');
                        result.push(c2);
                    }
                }
            }
            '$' => {
                match scanner.peek() {
                    // Consume the second '$'.  Turn '$$' into '$'.
                    '$' => result.push(scanner.read()),
                    // Otherwise, leave the $ as we found it.
                    // Don't consume the next character.
                    _ => result.push('$'),
                }
            }
            c => result.push(c),
        }
    }
    if start == scanner.ofs {
        return Ok(None);
    }
    return Ok(Some(result));
}

/// Parse a `.d` file into `Deps`.
pub fn parse<'a>(scanner: &mut Scanner<'a>) -> ParseResult<SmallMap<String, Vec<String>>> {
    let mut result = SmallMap::default();
    loop {
        while scanner.peek() == ' ' || scanner.peek_newline() {
            scanner.next();
        }
        let target = match read_path(scanner)? {
            None => break,
            Some(o) => o,
        };
        scanner.skip_spaces();
        let target = match target.strip_suffix(':') {
            None => {
                scanner.expect(':')?;
                target
            }
            Some(target) => target.to_string(),
        };
        let mut deps = Vec::new();
        while let Some(p) = read_path(scanner)? {
            deps.push(p);
        }
        result.insert(target, deps);
    }
    scanner.expect('\0')?;

    Ok(result)
}

#[cfg(test)]
mod tests {
    use crate::scanner::format_parse_error;

    use super::*;
    use std::path::Path;

    fn try_parse(buf: &mut Vec<u8>) -> Result<SmallMap<String, Vec<String>>, String> {
        buf.push(0);
        let mut scanner = Scanner::new(buf, 0);
        parse(&mut scanner).map_err(|err| format_parse_error(0, buf, Path::new("test"), err))
    }

    fn must_parse(buf: &mut Vec<u8>) -> SmallMap<String, Vec<String>> {
        match try_parse(buf) {
            Err(err) => {
                println!("{}", err);
                panic!("failed parse");
            }
            Ok(d) => d,
        }
    }

    fn test_for_crlf(input: &str, test: fn(String)) {
        let crlf = input.replace('\n', "\r\n");
        for test_case in [String::from(input), crlf] {
            test(test_case);
        }
    }

    #[test]
    fn test_parse() {
        test_for_crlf(
            "build/browse.o: src/browse.cc src/browse.h build/browse_py.h\n",
            |text| {
                let mut file = text.into_bytes();
                let deps = must_parse(&mut file);
                assert_eq!(
                    deps,
                    SmallMap::from([(
                        "build/browse.o".to_string(),
                        vec![
                            "src/browse.cc".to_string(),
                            "src/browse.h".to_string(),
                            "build/browse_py.h".to_string(),
                        ]
                    )])
                );
            },
        );
    }

    #[test]
    fn test_parse_space_suffix() {
        test_for_crlf("build/browse.o: src/browse.cc   \n", |text| {
            let mut file = text.into_bytes();
            let deps = must_parse(&mut file);
            assert_eq!(
                deps,
                SmallMap::from([(
                    "build/browse.o".to_string(),
                    vec!["src/browse.cc".to_string(),]
                )])
            );
        });
    }

    #[test]
    fn test_parse_multiline() {
        test_for_crlf(
            "build/browse.o: src/browse.cc\\\n  build/browse_py.h",
            |text| {
                let mut file = text.into_bytes();
                let deps = must_parse(&mut file);
                assert_eq!(
                    deps,
                    SmallMap::from([(
                        "build/browse.o".to_string(),
                        vec!["src/browse.cc".to_string(), "build/browse_py.h".to_string(),]
                    )])
                );
            },
        );
    }

    #[test]
    fn test_parse_without_final_newline() {
        let mut file = b"build/browse.o: src/browse.cc".to_vec();
        let deps = must_parse(&mut file);
        assert_eq!(
            deps,
            SmallMap::from([(
                "build/browse.o".to_string(),
                vec!["src/browse.cc".to_string(),]
            )])
        );
    }

    #[test]
    fn test_parse_spaces_before_colon() {
        let mut file = b"build/browse.o   : src/browse.cc".to_vec();
        let deps = must_parse(&mut file);
        assert_eq!(
            deps,
            SmallMap::from([(
                "build/browse.o".to_string(),
                vec!["src/browse.cc".to_string(),]
            )])
        );
    }

    #[test]
    fn test_parse_windows_dep_path() {
        let mut file = b"odd/path.o: C:/odd\\\\path.c".to_vec();
        let deps = must_parse(&mut file);
        assert_eq!(
            deps,
            SmallMap::from([(
                "odd/path.o".to_string(),
                vec!["C:/odd\\path.c".to_string(),]
            )])
        );
    }

    #[test]
    fn test_parse_escaped_space() {
        let mut file = b"odd/path.o: C:/odd\\ path.c".to_vec();
        let deps = must_parse(&mut file);
        assert_eq!(
            deps,
            SmallMap::from([("odd/path.o".to_string(), vec!["C:/odd path.c".to_string(),])])
        );
    }

    #[test]
    fn test_parse_final_backslash() {
        let mut file = b"path.o: path.c\\\\".to_vec();
        let deps = must_parse(&mut file);
        assert_eq!(
            deps,
            SmallMap::from([("path.o".to_string(), vec!["path.c\\".to_string(),])])
        );
    }

    #[test]
    fn test_parse_dollar_signs() {
        let mut file = b"odd$$path.o: C:/odd$path.c".to_vec();
        let deps = must_parse(&mut file);
        assert_eq!(
            deps,
            SmallMap::from([("odd$path.o".to_string(), vec!["C:/odd$path.c".to_string(),])])
        );
    }

    #[test]
    fn test_parse_multiple_targets() {
        let mut file = b"
out/a.o: src/a.c \\
  src/b.c

out/b.o :
"
        .to_vec();
        let deps = must_parse(&mut file);
        assert_eq!(
            deps,
            SmallMap::from([
                (
                    "out/a.o".to_string(),
                    vec!["src/a.c".to_string(), "src/b.c".to_string(),]
                ),
                ("out/b.o".to_string(), vec![])
            ])
        );
    }

    #[test]
    fn test_parse_missing_colon() {
        let mut file = b"foo bar".to_vec();
        let err = try_parse(&mut file).unwrap_err();
        assert!(
            err.starts_with("parse error: expected ':'"),
            "expected parse error, got {:?}",
            err
        );
    }
}
