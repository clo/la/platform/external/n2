//! Path canonicalization.

use std::mem::MaybeUninit;

/// An on-stack stack of values.
/// Used for tracking locations of parent components within a path.
struct StackStack<T> {
    n: usize,
    vals: [MaybeUninit<T>; 60],
}

impl<T: Copy> StackStack<T> {
    fn new() -> Self {
        StackStack {
            n: 0,
            // Safety: we only access vals[i] after setting it.
            vals: unsafe { MaybeUninit::uninit().assume_init() },
        }
    }

    fn push(&mut self, val: T) {
        if self.n >= self.vals.len() {
            panic!("too many path components");
        }
        self.vals[self.n].write(val);
        self.n += 1;
    }

    fn pop(&mut self) -> Option<T> {
        if self.n > 0 {
            self.n -= 1;
            // Safety: we only access vals[i] after setting it.
            Some(unsafe { self.vals[self.n].assume_init() })
        } else {
            None
        }
    }
}

/// Lexically canonicalize a path, removing redundant components.
/// Does not access the disk, but only simplifies things like
/// "foo/./bar" => "foo/bar".
/// These paths can show up due to variable expansion in particular.
/// Returns the new length of the path, guaranteed <= the original length.
#[must_use]
pub fn canon_path_fast(path: &mut str) -> usize {
    assert!(!path.is_empty());
    // Safety: this traverses the path buffer to move data around.
    // We maintain the invariant that *dst always points to a point within
    // the buffer, and that src is always checked against end before reading.
    unsafe {
        let mut components = StackStack::<*mut u8>::new();
        let mut dst = path.as_mut_ptr();
        let mut src = path.as_ptr();
        let start = path.as_mut_ptr();
        let end = src.add(path.len());

        if src == end {
            return 0;
        }
        if *src == b'/' || *src == b'\\' {
            src = src.add(1);
            dst = dst.add(1);
        }

        // Outer loop: one iteration per path component.
        while src < end {
            // Peek ahead for special path components: "/", ".", and "..".
            match *src {
                b'/' | b'\\' => {
                    src = src.add(1);
                    continue;
                }
                b'.' => {
                    let mut peek = src.add(1);
                    if peek == end {
                        break; // Trailing '.', trim.
                    }
                    match *peek {
                        b'/' | b'\\' => {
                            // "./", skip.
                            src = src.add(2);
                            continue;
                        }
                        b'.' => {
                            // ".."
                            peek = peek.add(1);
                            if !(peek == end || *peek == b'/' || *peek == b'\\') {
                                // Componet that happens to start with "..".
                                // Handle as an ordinary component.
                                break;
                            }
                            // ".." component, try to back up.
                            if let Some(ofs) = components.pop() {
                                dst = ofs;
                            } else {
                                *dst = b'.';
                                dst = dst.add(1);
                                *dst = b'.';
                                dst = dst.add(1);
                                if peek != end {
                                    *dst = *peek;
                                    dst = dst.add(1);
                                }
                            }
                            src = src.add(3);
                            continue;
                        }
                        _ => {}
                    }
                }
                _ => {}
            }

            // Mark this point as a possible target to pop to.
            components.push(dst);

            // Inner loop: copy one path component, including trailing '/'.
            while src < end {
                *dst = *src;
                src = src.add(1);
                dst = dst.add(1);
                if *src.offset(-1) == b'/' || *src.offset(-1) == b'\\' {
                    break;
                }
            }
        }

        if dst == start {
            *start = b'.';
            1
        } else {
            dst.offset_from(start) as usize
        }
    }
}

/// Canonicalizes the given path, by removing unnecessary components (like ./ or
/// foo/..)
pub fn canon_path<T: Into<String>>(path: T) -> String {
    let mut path = path.into();
    let len = canon_path_fast(&mut path);
    path.truncate(len);
    path
}

/// Escapes the shell-sensitive characters in the given string and appends the
/// result onto append_to.
/// 
/// TODO: Windows support. Though maybe not needed for android?
pub fn shell_escape(to_escape: &str, append_to: &mut String) {
    if !needs_shell_escaping(to_escape) {
        append_to.push_str(to_escape);
        return;
    }
    let num_quotes = to_escape.bytes().filter(|c| *c == b'\'').count();
    let estimated_size = 2 + to_escape.len() + 2 * num_quotes;
    let initial_len = append_to.len();
    append_to.reserve_exact(estimated_size);
    append_to.push('\'');
    let mut span_begin = 0;
    for (i, c) in to_escape.bytes().enumerate() {
        if c == b'\'' {
            append_to.push_str(&to_escape[span_begin..i]);
            append_to.push_str("'\\'");
            span_begin = i;
        }
    }
    append_to.push_str(&to_escape[span_begin..to_escape.len()]);
    append_to.push('\'');
    debug_assert_eq!(append_to.len(), initial_len + estimated_size);
}

fn needs_shell_escaping(to_escape: &str) -> bool {
    to_escape.chars().any(|c| !is_known_shell_safe_character(c))
}

fn is_known_shell_safe_character(c: char) -> bool {
    matches!(c, 'A'..='Z' | 'a'..='z' | '0'..='9' | '_' | '+' | '-' | '.' | '/')
}

#[cfg(test)]
mod tests {
    use super::*;

    // Assert that canon path equals expected path with different path separators
    fn assert_canon_path_eq(left: &str, right: &str) {
        assert_eq!(canon_path(left), right);
        assert_eq!(
            canon_path(left.replace('/', "\\")),
            right.replace('/', "\\")
        );
    }

    #[test]
    fn noop() {
        assert_canon_path_eq("foo", "foo");

        assert_canon_path_eq("foo/bar", "foo/bar");
    }

    #[test]
    fn dot() {
        assert_canon_path_eq("./foo", "foo");
        assert_canon_path_eq("foo/.", "foo/");
        assert_canon_path_eq("foo/./bar", "foo/bar");
        assert_canon_path_eq("./", ".");
        assert_canon_path_eq("./.", ".");
        assert_canon_path_eq("././", ".");
        assert_canon_path_eq("././.", ".");
        assert_canon_path_eq(".", ".");
    }

    #[test]
    fn slash() {
        assert_canon_path_eq("/foo", "/foo");
        assert_canon_path_eq("foo//bar", "foo/bar");
    }

    #[test]
    fn parent() {
        assert_canon_path_eq("foo/../bar", "bar");

        assert_canon_path_eq("/foo/../bar", "/bar");
        assert_canon_path_eq("../foo", "../foo");
        assert_canon_path_eq("../foo/../bar", "../bar");
        assert_canon_path_eq("../../bar", "../../bar");
        assert_canon_path_eq("./../foo", "../foo");
        assert_canon_path_eq("foo/..", ".");
        assert_canon_path_eq("foo/../", ".");
        assert_canon_path_eq("foo/../../", "../");
        assert_canon_path_eq("foo/../../bar", "../bar");
    }
}
