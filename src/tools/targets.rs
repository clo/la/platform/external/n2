use crate::graph::File;
use crate::load;
use std::collections::BTreeSet;
use std::sync::Arc;
use anyhow::bail;

// Implements the "targets" tool.
//
// The targets rule is rather convoluted. It has 3 modes. The mode to use is
// the first argument, and the default is "depth".
//   - depth: prints a tree of files and their dependencies, starting from all
//            of the root nodes in the graph. An argument can be given to
//            specify the maximum depth to print out, the default is 1. A max
//            depth of 0 means "infinity".
//   - rule: the rule mode takes an argument that's the name of a rule. It will
//           print out all the files produced by builds using that rule. If the
//           rule argument is not given, it will instead print out all
//           "source files" in the graph, that is, files that are not produced
//           by any build.
//   - all: prints out the output files of all builds and the name of the rule
//          used to produce them.
pub fn tool_targets(build_file: &str, args: &Vec<String>) -> anyhow::Result<u8> {
    let state = load::read(build_file)?;
    match args.first().map(|x| x.as_str()) {
        Some("all") => tool_targets_all(&args[1..], state)?,
        Some("rule") => tool_targets_rule(&args[1..], state)?,
        Some("depth") => tool_targets_depth(&args[1..], state)?,
        None => tool_targets_depth(args, state)?,
        Some(mode) => bail!("unknown target tool mode {:?}, valid modes are \"rule\", \"depth\", or \"all\".", mode),
    }
    Ok(0)
}

fn tool_targets_all(args: &[String], state: load::State) -> anyhow::Result<()> {
    if !args.is_empty() {
        bail!("too many arguments to targets tool");
    }
    for build in state.graph.builds.values() {
        for file in &build.outs.ids {
            println!("{}: {}", file.name, build.rule)
        }
    }
    Ok(())
}

fn tool_targets_rule(args: &[String], state: load::State) -> anyhow::Result<()> {
    match args.len() {
        0 => {
            let mut results = BTreeSet::new();
            for build in state.graph.builds.values() {
                for file in &build.ins.ids {
                    if file.input.lock().unwrap().is_none() {
                        results.insert(file.name.clone());
                    }
                }
            }
            for result in &results {
                println!("{}", result);
            }
        },
        1 => {
            let rule = &args[0];
            let mut results = Vec::new();
            for build in state.graph.builds.values() {
                if rule == &build.rule {
                    for file in &build.outs.ids {
                        results.push(file.name.clone());
                    }
                }
            }
            results.sort_unstable();
            for result in &results {
                println!("{}", result);
            }
        },
        _ => bail!("too many arguments to targets tool"),
    };
    Ok(())
}

fn tool_targets_depth(args: &[String], state: load::State) -> anyhow::Result<()> {
    let max_depth = match args.len() {
        0 => 1,
        1 => args[0].parse::<i32>()?,
        _ => bail!("too many arguments to targets tool"),
    };
    // The graph contains file entries for included ninja files, so
    // we only consider files that are produced by a build.
    let mut root_nodes = Vec::new();
    for build in state.graph.builds.values() {
        for file in &build.outs.ids {
            if file.dependents.is_empty() {
                root_nodes.push(file.clone());
            }
        }
    }
    // sort the root nodes because the builds in the graph are in a nondeterministic order
    root_nodes.sort_unstable_by(|f1, f2| f1.name.cmp(&f2.name));
    print_files_recursively(&state, &root_nodes, 0, max_depth);
    Ok(())
}

/// print all the files in the `files` argument, and then all of their
/// dependencies, recursively. `depth` is used to determine the indentation,
/// start it at 0. max_depth limits the recursion depth, but if it's <= 0,
/// recursion depth will not be limited.
fn print_files_recursively(state: &load::State, files: &[Arc<File>], depth: i32, max_depth: i32) {
    for file in files {
        for _ in 0..depth {
            print!("  ");
        }
        if let Some(build_id) = *file.input.lock().unwrap() {
            let build = state.graph.builds.lookup(build_id).unwrap();
            println!("{}: {}", &file.name, build.rule);
            if max_depth <= 0 || depth < max_depth - 1 {
                print_files_recursively(state, build.ordering_ins(), depth+1, max_depth);
            }
        } else {
            println!("{}", &file.name);
        }
    }
}
