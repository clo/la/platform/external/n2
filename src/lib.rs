//! n2 is a rust rewrite of the ninja build system.
//! Its primary distinguishing feature is that it considers actions out of
//! date if a hash of their metadata doesn't match the result of the last build,
//! rather than trying to track individual file mtimes.

pub mod canon;
mod concurrent_linked_list;
mod db;
mod densemap;
mod depfile;
mod eval;
mod file_pool;
mod graph;
mod hash;
pub mod load;
pub mod parse;
mod process;
#[cfg(unix)]
mod process_posix;
#[cfg(windows)]
mod process_win;
mod progress;
pub mod run;
pub mod scanner;
mod signal;
mod smallmap;
mod task;
mod terminal;
mod tools;
mod trace;
mod work;

#[cfg(target_os = "linux")]
#[global_allocator]
static GLOBAL: tikv_jemallocator::Jemalloc = tikv_jemallocator::Jemalloc;
