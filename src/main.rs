//! The n2 binary crate, it mostly just defers to the run function in the
//! library crate.

use log::debug;

fn main() -> std::process::ExitCode {
    env_logger::init();

    let exit_code = match n2::run::run() {
        Ok(code) => code,
        Err(err) => {
            println!("n2: error: {}", err);
            1
        }
    };
    debug!("Exiting from main with exit code {}", exit_code);
    exit_code.into()
}
